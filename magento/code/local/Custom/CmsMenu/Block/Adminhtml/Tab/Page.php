<?php

class Custom_CmsMenu_Block_Adminhtml_Tab_Page
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function __construct()
    {
        parent::__construct();
        $this->setShowGlobalIcon(true);
    }

    protected function _prepareForm()
    {

        if ($this->_isAllowedAction('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('page_');

        $model = Mage::registry('cms_page');

        $layoutFieldset = $form->addFieldset('layout_fieldset_menus', array(
            'legend' => Mage::helper('customcmsmenu')->__('Menus'),
            'disabled'  => $isElementDisabled
        ));
        
        $layoutFieldset->addField('menu_id', 'select', array(
            'name'     => 'menu_id',
            'label'    => Mage::helper('customcmsmenu')->__('Select menu'),
            'required' => true,
            'values'   => Mage::getSingleton('customcmsmenu/menus')->toOptionArray(),
            'disabled' => $isElementDisabled,
            'note'      => Mage::helper('customcmsmenu')->__('Use page lyaout with left column to view the menu'),
        ));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return Mage::helper('customcmsmenu')->__('Menus');
    }

    public function getTabTitle()
    {
        return Mage::helper('customcmsmenu')->__('Menus');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/page/' . $action);
    }
}
