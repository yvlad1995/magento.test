<?php

class Custom_CmsMenu_Block_Adminhtml_Cmslinks_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct($arguments=array())
    {
        parent::__construct();
        $this->setId('grid');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() 
    {
        $collection = Mage::getModel('customcmsmenu/cmslinks')
                ->getCollection()
                ->addFieldToFilter('menu_id', $this->getRequest()->getParam('id'));
  
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns() 
    {   
        $helper = Mage::helper('customcmsmenu');

        $this->addColumn('links_id', array(
            'header' => $helper->__('ID'),
            'index'  => 'links_id',
            'type'   => 'number'
        ));

        $this->addColumn('label', array(
            'header' => $helper->__('Name'),
            'index'  => 'label',
            'type'   => 'text',
        ));
        
        $this->addColumn('url_key', array(
            'header' => $helper->__('URL Key'),
            'index'  => 'url_key',
            'type'   => 'text',
        ));

        $this->addColumn( 'type', array(
            'header' => $helper->__('Type'),
            'index'  => 'type',
            'type'   => 'options',
            'options' => array(
                '0'  => 'The same frame',
                '1' => 'New window',
            )
        ));

        $this->addColumn( 'position', array(
            'header'  => $helper->__('Position'),
            'index'   => 'position',
            'type'    => 'text',
        ));

        return parent::_prepareColumns();
    }
    
    public function getRowUrl($model)
    {
        return $this->getUrl('*/adminhtml_cmslinks/edit', array(
            'link_id' => $model->getId(),
            'menu_id' => $model->getMenuId()
        ));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/lookGrid', array('_current'=>true));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('links_id');
        $this->getMassactionBlock()->setFormFieldName('link');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('customcmsmenu')->__('Delete'),
            'confirm' => Mage::helper('customcmsmenu')->__('Are you sure?'),
            'url' => $this->getUrl('*/adminhtml_cmslinks/massDelete', array(
                'id' => $this->getRequest()->getParam('id')
            )),

        ));
        return parent::_prepareMassaction();
    }

}