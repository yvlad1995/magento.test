<?php

class Custom_CmsMenu_Block_Adminhtml_Cmslinks_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $helper = Mage::helper('customcmsmenu');
        $model = Mage::registry('current_cmslinks');

        $form = new Varien_Data_Form(array(
            'id'     => 'edit_form',
            'action' => $this->getUrl('*/*/save', array(
                'link_id'     => $this->getRequest()->getParam('link_id'),
                'menu_id'     => $this->getRequest()->getParam('menu_id')
            )),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $helper->__('General Information')
        ));

        $fieldset->addField('label', 'text', array(
            'label' => $helper->__('Label'),
            'required' => true,
            'name' => 'label',
        ));

        $fieldset->addField('url_key', 'text', array(
            'label' => $helper->__('Url Key'),
            'required' => true,
            'name' => 'url_key',
            'note'      => Mage::helper('customcmsmenu')->__('Relative to Website Base URL'),
        ));

        $fieldset->addField('position', 'text', array(
            'label' => $helper->__('Position'),
            'required' => true,
            'name' => 'position',
        ));

        $fieldset->addField('type', 'select', array(
            'label' => $helper->__('Type'),
            'default' => 0,
            'name' => 'type',
            'type' => 'options',
            'options' => array(
                '0'  => 'The same frame',
                '1'  => 'New window',
            )
        ));

        $form->setUseContainer(true);
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}