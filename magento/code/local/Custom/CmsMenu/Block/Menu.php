<?php

class Custom_CmsMenu_Block_Menu extends Mage_Core_Block_Template
{

   public function _prepareLayout()
   {
       return parent::_prepareLayout();
   }

    public function getMenuCollection()
    {
        $id = Mage::getSingleton('cms/page')->getData('menu_id'); //get menu id on page
        $status = Mage::getSingleton('customcmsmenu/cmsmenu')//get status menu
        ->load($id, 'id')
            ->getStatus();

        $store_view = Mage::getStoreConfig('customcmsmenu/customcmsmenu_group/customcmsmenu_select', Mage::app()->getStore());

        $links = array();
        if ($status == '1' && $store_view == '1')
        {
            $links = Mage::getSingleton('customcmsmenu/cmslinks')->getCollection()->addFieldToFilter('menu_id', $id)->getData();
        }

        return $links;
    }
}
