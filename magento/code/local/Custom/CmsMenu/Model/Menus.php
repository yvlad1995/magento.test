<?php

class Custom_CmsMenu_Model_Menus
{
    protected $_options = null;

//    protected $_defaultValue = null;

    public function getOptions()
    {
        if ($this->_options === null) {
            $this->_options = array();
            //Переписать!
            $this->_options[0] = 'None';
            
            foreach (Mage::getSingleton('customcmsmenu/cmsmenu')->getCollection()->getData() as $layout) {
                if($layout['status'] == 0){
                    $this->_options[$layout['id']] = $layout['menu_name'] . ' (disabled)';
                }else{
                    $this->_options[$layout['id']] = $layout['menu_name'];
                }
            }
        }
        return $this->_options;
    }

    public function toOptionArray($withEmpty = false)
    {
        $options = array();

        foreach ($this->getOptions() as $value => $label) {
            $options[] = array(
                'label'  => $label,
                'value'  => $value,
            );
        }

        if ($withEmpty) {
            array_unshift($options, array('value'=>'', 'label'=>Mage::helper('customcmsmenu')->__('-- Please Select --')));
        }

        return $options;
    }

}