<?php

class Custom_CmsMenu_Model_System_Store
{

    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('customcmsmenu')->__('Enable')),
            array('value' => 0, 'label'=>Mage::helper('customcmsmenu')->__('Disable')),
        );
    }

    public function toArray()
    {
        return array(
            0 => Mage::helper('customcmsmenu')->__('Disable'),
            1 => Mage::helper('customcmsmenu')->__('Enable'),
        );
    }
}