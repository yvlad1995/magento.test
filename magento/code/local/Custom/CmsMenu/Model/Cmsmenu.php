<?php

class Custom_CmsMenu_Model_Cmsmenu extends Mage_Core_Model_Abstract
{
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('customcmsmenu/cmsmenu');
    }
}
